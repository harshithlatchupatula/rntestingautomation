
exports.config = {
    services: ['appium'],
    port: 4723,
    runner: 'local',
    specs: [
      './App.test.js'
    ],
    path: '/wd/hub',
    capabilities: [{
      platformName: "iOS",
      platformVersion: "13.6",
      deviceName: "iPhone 6s",
      app: "/Users/Harshith/Desktop/RNTestingMBP/Detox/androidDetoxTest/ios/build/Build/Products/Debug-iphonesimulator/androidDetoxTest.app",
      appPackage: "com.androiddetoxtest",
      automationName: "XCUITest",
      xcodeOrgId: "Q49N3A77JA",
      xcodeSigningId: "iPhone Developer",
      udid: "9ddd0d71fc6de067093c32f673518db35a09b251"
    }],
    
    logLevel: 'trace',
    bail: 0,
    waitforTimeout: 10000,
    connectionRetryTimeout: 90000,
    connectionRetryCount: 3,
    framework: 'mocha',
    reporters: ['spec'],
    mochaOpts: {
      ui: 'bdd',
    }
  }