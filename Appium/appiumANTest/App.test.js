var expect = require('chai').expect;

describe('Simple App testing', () => {

  // Adding time out to make sure the app is load prior to test is run
  beforeEach(() => {
    $("~emailAddress").waitForDisplayed(11000, false)
  });

  it('Valid Login Test', async => {
    $('~emailAddress').setValue("shamique");
    $('~password').setValue("123456");

    const status = $("~emailAddress").getText();
    expect(status).to.equal('shamique');
  });


});