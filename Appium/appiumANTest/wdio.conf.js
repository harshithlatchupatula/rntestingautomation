
exports.config = {
  services: ['appium'],
  port: 4723,
  runner: 'local',
  specs: [
    './App.test.js'
  ],
  path: '/wd/hub',
  capabilities: [{
    platformName: "Android",
    platformVersion: "9",
    deviceName: "Pixel_3a_XL_API_28",
    app: "/Users/Harshith/Desktop/RNTestingMBP/Detox/androidDetoxTest/android/app/build/outputs/apk/debug/app-debug.apk",
    appPackage: "com.androiddetoxtest",
    automationName: "UiAutomator2"
  }],
  
  logLevel: 'trace',
  bail: 0,
  waitforTimeout: 10000,
  connectionRetryTimeout: 90000,
  connectionRetryCount: 3,
  framework: 'mocha',
  reporters: ['spec'],
  mochaOpts: {
    ui: 'bdd',
  }
}