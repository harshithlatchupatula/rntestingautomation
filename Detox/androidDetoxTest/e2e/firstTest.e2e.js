describe('Login flow test', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('Login screen should appear', async () => {
    await expect(element(by.text('Forgot your password?'))).toBeVisible();
  });

  it('Login functionality should work', async () => {
    await element(by.id('emailAddress')).typeText('sampleaddress@gmail.com');
    await element(by.id('password')).typeText('test123');
    await element(by.text('Login')).tap();
    await expect(element(by.text('Button pressed login'))).toBeVisible();
  });
});
