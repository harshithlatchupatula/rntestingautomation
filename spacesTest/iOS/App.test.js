var expect = require('chai').expect;

describe('Simple App testing', () => {

  beforeEach(() => {
    driver.reset();
  });

  it('Student account log in functionality', async => {
    $('~student').click();
    $('~emailInput').setValue("test@myblueprint.org");
    $('~passwordInput').setValue("test");
    $('[name="Log In"]').click();
    const status = $("~loginstatus").isDisplayed();
    expect(status).to.equal('fail');
  });

  it('Teacher log in functionality', async => {
    $('~teacher').click();
    $('~emailInput').setValue("test@myblueprint.org");
    $('~passwordInput').setValue("test");
    $('[name="Log In"]').click();
  });

  it('Family log in functionality', async => {
    $('~family').click();
    $('~emailInput').setValue("test@myblueprint.org");
    $('~passwordInput').setValue("test");
    $('[name="Log In"]').click();
  });

});