exports.config = {
    services: ['appium'],
    port: 4723,
    runner: 'local',
    specs: [
      './App.test.js'
    ],
    path: '/wd/hub',
    capabilities: [{
      platformName: "iOS",
      platformVersion: "13.6",
      deviceName: "iPhone 11",
      app: "/Users/Harshith/Desktop/myblueprint-nativeapp/ios/build/Build/Products/Debug-iphonesimulator/Spaces.app",
      automationName: "XCUITest",
    }],
    
    logLevel: 'trace',
    bail: 0,
    waitforTimeout: 90000,
    connectionRetryTimeout: 90000,
    connectionRetryCount: 3,
    framework: 'mocha',
    reporters: ['spec'],
    mochaOpts: {
      ui: 'bdd',
      timeout: 90000,
    }
  }